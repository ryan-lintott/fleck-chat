using Microsoft.Extensions.Logging;
using System.Net;
using System;
using System.Threading.Tasks;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;


namespace ChatSample {
    public class Program {
        public static int Main(string[] args) {
            var loggerFactory = LoggerFactory.Create(builder => {
                builder.AddConsole();
            });

            var logger = loggerFactory.CreateLogger<Program>();
            AgonesSdkService agonesSdkService = new AgonesSdkService(loggerFactory.CreateLogger<AgonesSdkService>());

            agonesSdkService.StartAsync().Wait();

            RoomsManager roomsManager = new RoomsManager();

            for(int i = 0; i < 25; i++) {
                roomsManager.CreateRoom();
            }
            
            var server = new FleckServerChatNS.FleckChatServer("wss://0.0.0.0:5000", 
                                                               X509Certificate2.CreateFromEncryptedPemFile("wss/cert.pem", "qwerty", "wss/key.pem"),
                                                               roomsManager, 
                                                               loggerFactory.CreateLogger<FleckServerChatNS.FleckChatServer>());
            
            
            server.Start();
            logger.LogInformation("started fleck server...");
            Task.Delay(-1).Wait();

            // 

            return 0;
        }
    }
}