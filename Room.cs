using System.Collections.Concurrent;
using Fleck;
class Room {

    // TODO: the c# get, set syntax
    ConcurrentDictionary<string, IWebSocketConnection> playerIdToConnectionId = new ConcurrentDictionary<string, IWebSocketConnection>();
    // TODO: connectionId to playerId

    public const int maxPlayers = 4;

    public int GetPlayerCount() {
        return playerIdToConnectionId.Count;
    }

    public bool AddOrUpdatePlayer(string playerId, IWebSocketConnection connectionId) {
        //lock(playerIdToConnectionId) {
            if(playerIdToConnectionId.Count >= maxPlayers && !playerIdToConnectionId.ContainsKey(playerId)) {
                // TODO: custom exceptions maybe
                throw new Exception("exceeded max player count");
            }
            playerIdToConnectionId[playerId] = connectionId;   
        //}
        return true;
    }

    public bool RemoveConnection(IWebSocketConnection connectionId) {
        bool result = false;

        foreach(var keyValue in playerIdToConnectionId) {
            if(keyValue.Value != null && keyValue.Value.Equals(connectionId)) {
                // TODO: remove player
                result = playerIdToConnectionId.TryUpdate(keyValue.Key, null, connectionId);
                break;
            }
        }
        return result;
    }

    public List<IWebSocketConnection> GetConnections() {
        return playerIdToConnectionId.Values.ToList();
    }
}