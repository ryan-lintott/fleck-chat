using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Fleck;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;

namespace FleckServerChatNS {

    public class Command {
        public string type;
        public string roomId;
        public string user;
        public string message;
    }

    public class FleckChatServer {

        RoomsManager _roomsManager;

        private ILogger<FleckChatServer> _logger;

        WebSocketServer fleckServer;
        public FleckChatServer(string address, X509Certificate2 certificate, RoomsManager roomsManager, ILogger<FleckChatServer> logger) {
            fleckServer = new WebSocketServer(address);
            _roomsManager = roomsManager;
            _logger = logger;
           fleckServer.Certificate = certificate;
           fleckServer.EnabledSslProtocols = SslProtocols.Tls12;
        }

        public void Start() {
            fleckServer.Start(socket => {
                
                socket.OnOpen = () => {
                    _logger.LogInformation("connected");
                };
                
                socket.OnClose = () => {
                    byte b = 0;
                    _roomsManager.currentConnections.TryRemove(socket, out b);
                    string? roomId = _roomsManager.RemoveConnection(socket);
                    if(roomId != null) {
                        var sessions = _roomsManager.GetConnections(roomId);
                        if(sessions != null) {
                            // await Clients.Clients(sessions)
                            //          .SendAsync("AdminMessage", $"{Context.ConnectionId} disconnected");
                        }
            }  
                };
                
                socket.OnMessage = message => {

                    Command command = new Command();
                    command.type = "ReceiveMessage";
                    
                    // Multicast message to all connected sessions
                    var received = JsonConvert.DeserializeObject<Command>(message);
                    List<IWebSocketConnection> connections = _roomsManager.GetConnections(received.roomId);

                    if(received.type.Equals("Send")) {
                        command.message = received.message;
                        command.user = received.user;
                        command.roomId = received.roomId;

                        foreach(IWebSocketConnection conn in connections) {
                            try {

                                if(conn != null && conn.IsAvailable) {
                                    conn.Send(JsonConvert.SerializeObject(command));
                                }
                                            
                            } catch(Exception e) {
                                // Console.WriteLine(e.Message);
                                _logger.LogWarning(e.Message);
                            }
                        }

                    } else if((received.type.Equals("JoinRoom"))) {
                        _roomsManager.AddOrUpdatePlayer(received.roomId, 
                                                        received.user, 
                                                        socket);
                    } else {
                        // TODO: error or throw exception
                    }
                    
                };

                socket.OnError = err => {
                     _logger.LogError($"Chat WebSocket session caught an error with code {err}");
                };


            });
        }
        
    }

}